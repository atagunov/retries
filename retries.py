# -*- coding: utf-8 -*-
import time
from contextlib import contextmanager


def retries(timeout, sleep_time=1, exception=AssertionError):
    """
    This generator function yields a context manager for timeout seconds with sleep_time interval.
    Exception is suppressed inside the context manager if timeout is not exceeded. Otherwise exception
    is reraised as usual.

    :param timeout:
    :param sleep_time: interval between iterations (in seconds)
    :param exception:
    :return: The generator function that yields the context manager.

    For example::

        for r in self.retries(timeout=60):
            with r:
                self.assertTrue(self.is_email_confirmed())

    The code under context manager will continue to be called for timeout seconds
    until self.is_email_confirmed() is True.
    If self.is_email_confirmed() won't go True for 60 seconds, it will raise a common assertion error.

    """
    timeout_at = time.time() + timeout
    state = {"fails_count": 0, "give_up": False, "success": False}
    while time.time() < timeout_at:
        yield _handler(exception, state)
        if state["success"]:
            return
        time.sleep(sleep_time)
    state["give_up"] = True
    yield _handler(exception, state)


@contextmanager
def _handler(exception, state):
    try:
        yield
    except exception:
        state["fails_count"] += 1
        if state["give_up"]:
            raise
    else:
        state["success"] = True

