from setuptools import setup

setup(
    name="retries",
    version="0.1",
    author="Anton Tagunov",
    url="https://bitbucket.org/atagunov/retries/",
    description="Write easy retry code",
    long_description=open('README.rst').read(),
    install_requires=['six'],
    test_suite="test",
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: MIT License",
        "Environment :: Other Environment",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Libraries :: Python Modules"]
)
