# -*- coding: utf-8 -*-
from unittest import TestCase
from retries import retries


class TestRetries(TestCase):
    def test_success_retries(self):
        count = 0
        for r in retries(timeout=2, sleep_time=0.001):
            with r:
                count += 1
                self.assertEqual(count, 4)

    def test_failed_retries(self):
        count = 0
        with self.assertRaises(AssertionError):
            for r in retries(timeout=1, sleep_time=0.001):
                with r:
                    count += 1
                    self.assertLess(count, 0)
